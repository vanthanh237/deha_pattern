<?php

interface Target
{
    public function send($message);
}

class Adaptee
{
    public function receive($message): void
    {
        echo 'Adapter Pattern :' . PHP_EOL;
        echo $message . PHP_EOL;
    }
}

class Adapter implements Target
{
    protected $adaptee;

    public function __construct(Adaptee $adaptee)
    {
        $this->adaptee = $adaptee;
    }

    private function translate($message)
    {
        return "Hello";
    }

    public function send($message)
    {
        $EMessage = $this->translate($message);
        $this->adaptee->receive($EMessage);
    }
}


class Client
{
    public function send()
    {
        $target = new Adapter(new Adaptee);
        $target->send('Xin chào');
    }
}

$client = new Client;
$client->send();